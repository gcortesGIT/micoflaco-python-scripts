from datetime import datetime
from influxdb import InfluxDBClient
import yaml


import influxdb
import requests
import xlwings as xw

# Opens my Excel Spreadsheet
WB = xw.Book(r'C:\Documents\Excelfile1.xlsx')
SH = WB.sheets['Sheet1']

# ====================================
# === Reading YML files with info  ===
# ====================================

def sending_list():
    """Reading the YML files to collect info to query Grafana"""
    try:
        lista = yaml.full_load(open('/data_nbn.yml', "r"))
        list_type = 'nbn'
        entries = collect_data_grafana(lista)
        insert_xls(list_type, entries)
        lista = yaml.full_load(open('/data_dsl.yml', "r"))
        list_type = 'dsl'
        entries = collect_data_grafana(lista)
        insert_xls(list_type, entries)
        lista = yaml.full_load(open('/data_eth.yml', "r"))
        list_type = 'eth'
        entries = collect_data_grafana(lista)
        insert_xls(list_type, entries)

    except FileNotFoundError:
        print('ERROR!, Wrong inventory file name or file path')

# ====================================
# === Collecting Data from Grafana ===
# ====================================

def collect_data_grafana(lista):
    """Collects Data from Grafana DB"""
    mysql_entries = []
    influxdb_config = {
        'host':     '192.168.0.1',
        'port':     8086,
        'username': 'someuser',
        'password': 'somepasswd',
        'database': 'MYDB',
        'timeout':  10,
        'retries':  3,
    }
    try:
        client_abc = InfluxDBClient(**influxdb_config)
        client_abc.ping()
        for host, dets in lista.items():
            last_period = '30d'
            hostname = host
            if 'optional' in dets.keys():
                if 'last_period' in dets['optional'].keys():
                    last_period = dets['optional']['last_period']
            for portname in dets['portname']:
                mysql_entry = ['hostname', 'portname', 'maxrx', 'maxtx', 'last_period', 'date']
                mysql_entry[0] = hostname
                mysql_entry[4] = last_period
                if '|' in portname:
                    mysql_entry[1] = portname
                    portname = portname.replace('|', '\\|')
                query_max_rx = f"select max(\"metric1\") as maxrx from (SELECT \
                non_negative_derivative(max(\"value\"), 1s) *8/1000000 as metric1 FROM \"snmp_rx\" WHERE \
                (\"host\" = '{hostname}' AND \"type_instance\" =~ /{portname}.*/ AND \"type\" = \
                'if_octets' ) AND time >= now() - {last_period} GROUP BY time(1s), \
                \"host\",\"type_instance\" fill(null))"
                query_max_tx = f"select max(\"metric1\") as maxtx from (SELECT \
                non_negative_derivative(max(\"value\"), 1s) *8/1000000 as metric1 FROM \"snmp_tx\" WHERE \
                (\"host\" = '{hostname}' AND \"type_instance\" =~ /{portname}.*/ AND \"type\" = \
                'if_octets' ) AND time >= now() - {last_period} GROUP BY time(1s), \
                \"host\",\"type_instance\" fill(null))"
                mysql_entry[1] = portname.replace("_", "/").replace('\\|', '|')
                queries = (query_max_rx, query_max_tx)
                for query in queries:
                    result = client_abc.query(query)
                    if result:
                        if 'maxrx' in list(result.get_points())[0]:
                            maxrx = "{:.2f}".format(list(result.get_points())[0]['maxrx'])
                            mysql_entry[2] = float(maxrx)
                        elif 'maxtx' in list(result.get_points())[0]:
                            maxtx = "{:.2f}".format(list(result.get_points())[0]['maxtx'])
                            mysql_entry[3] = float(maxtx)
                    else:
                        maxrx = maxtx = 0.0
                if maxrx and maxtx:
                    date = datetime.now().strftime("%Y-%m-%d %I:%M:%S")
                    mysql_entry[5] = date
                    mysql_entries.append(mysql_entry)
        return mysql_entries
    except FileNotFoundError:
        print('ERROR!, Wrong inventory file name or file path')
    except requests.exceptions.ConnectTimeout:
        print('ERROR!, Timeout connecting to Grafana DB')
    except requests.exceptions.ReadTimeout:
        print('ERROR!, Timeout Reading from Grafana DB')
    except influxdb.exceptions.InfluxDBClientError:
        print('ERROR!, cannot read from Grafana DB, check its name')

# ==================================
# === Filling the Excel Sheet    ===
# ==================================

def insert_xls(list_type, entries):
    """Filling Sheets with new data for NBN"""
    num = 5
    if list_type == 'nbn':
        for i in entries:
            SH.range('A'+ str(num)).value = i
            num += 1
    if list_type == 'dsl':
        for i in entries:
            SH.range('H'+ str(num)).value = i
            num += 1
    if list_type == 'eth':
        for i in entries:
            SH.range('O'+ str(num)).value = i
            num += 1

# ==================================
# ==============MAIN==============
# ==================================

if __name__ == "__main__":
    T1 = datetime.now()
    sending_list()
    T2 = datetime.now()
    print(f"Completed in {T2 - T1}")
