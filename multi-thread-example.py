"""Imports"""
import datetime
import telnetlib
# import multiprocessing as mp
import threading
import yaml
from datetime import datetime
from netmiko import ConnectHandler



def main():
    """We want to know how much time will the program take"""
    start_time = datetime.now()
    threads = []
    devices = get_nodes()
    # We need to assign number of process equal to number of devices
    # (one process will connect to one device and execute the command)
    # and set the target of the process to execute the function
    # we defined previously (ssh_session).
    for key in devices:
        node = devices[key]
        d_type = node['device_type']
        ip_add = node['ip']
        username = node['username']
        password = node['password']
        threads.append(threading.Thread(target=ssh_session, \
        args=(key, d_type, ip_add, username, password)))

    # We need to start the threads
    for current_threads in threads:
        current_threads.start()

    # Then we end the Threads
    main_thread = threading.currentThread()
    for current_threads in threads:
        if current_threads != main_thread:
            current_threads.join()

    # Accessing an specific device through Telnet (outside multi-thread)
    data = the_old_10k()
    temp10k = len(data)
    print('{} = {}'.format('HOST1', temp10k))

    # Want to know how much time did the program take
    print("\nElapsed time: " + str(datetime.now() - start_time))

# ==================================
# ==========ROUTERS INPUT===========
# ==================================

def get_nodes():
    """This function reads device details from a Yaml file and returns in result var"""
    with open("./routers.yml") as nodes:
        result = yaml.safe_load(nodes)
        return result

# ==================================
# ======CONNECTING ALL DEVICES======
# ==================================

def ssh_session(router, d_type, ip_add, username, password):
    """Connects to a device via SSH using Netmiko"""
    output = []
    connect = ConnectHandler(device_type=d_type, ip=ip_add, \
    username=username, password=password, fast_cli=True)
    output = connect.send_command('show interfaces description | i up')
    total = len(output.splitlines())
    print('{} = {}'.format(router, total))


def the_old_10k():
    """This function is created to access a device that cannot run SSH"""
    host = "192.168.1.1"
    user = "admin"
    password = "somepassword"
    cmd1 = "term len 0\n"
    cmd2 = "show interfaces description | i up\n"

    tln = telnetlib.Telnet(host, timeout=10)
    tln.expect([b"Username: "])
    tln.write(user.encode('ascii') + b"\n")
    if password:
        tln.expect([b"Password: "])
        tln.write(password.encode('ascii') + b"\n")

    tln.write(cmd1.encode('ascii'))
    tln.expect([b"#"])
    tln.write(cmd2.encode('ascii'))
    tln.expect([b"#"])
    data = tln.read_until(b'#').decode('ascii')
    return data.splitlines()

# ==================================
# ==============MAIN==============
# ==================================
if __name__ == '__main__':
    main()
